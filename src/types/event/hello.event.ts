import { GatewayEvent } from '../gateway/event';
import { Event } from './event';

export class HelloEvent extends Event<HelloPayload> {
    code: number = Number.parseInt(GatewayEvent.HelloEvent);

    constructor(payload: HelloPayload) {
        super(payload);
    }
}

interface HelloPayload {
    heartbeat_interval: number;
}
