import { Event, UNKNOWN_EVENT } from '@/types/event/event';
import { HeartbeatAckEvent } from '@/types/event/heartbeat-ack.event';
import { HeartbeatEvent } from '@/types/event/heartbeat.event';
import { HelloEvent } from '@/types/event/hello.event';
import { IdentifyEvent } from '@/types/event/identify.event';
import { GatewayPayload } from '@/types/gateway/payload';
import { GatewayEvent } from '@/types/gateway/event';
import { ResumeEvent } from '@/types/event/resume.event';
import { ReconnectEvent } from '@/types/event/reconnect.event';
import { ReadyEvent } from '@/types/event/ready.event';
import { MemberUpdateEvent } from '@/types/event/member/member.update.event';
import { MessageCreateEvent } from '@/types/event/message/message.create.event';
import { MessageUpdateEvent } from '@/types/event/message/message.update.event';
import { MessageDeleteEvent } from '@/types/event/message/message.delete.event';

export function parseEvent(payload: GatewayPayload): Event<any> {
    const EventType = {};
    EventType[GatewayEvent.ReadyEvent] = ReadyEvent;
    EventType[GatewayEvent.MemberUpdateEvent] = MemberUpdateEvent;
    EventType[GatewayEvent.MessageCreateEvent] = MessageCreateEvent;
    EventType[GatewayEvent.MessageUpdateEvent] = MessageUpdateEvent;
    EventType[GatewayEvent.MessageDeleteEvent] = MessageDeleteEvent;
    EventType[GatewayEvent.HeartbeatEvent] = HeartbeatEvent;
    EventType[GatewayEvent.IdentifyEvent] = IdentifyEvent;
    EventType[GatewayEvent.ResumeEvent] = ResumeEvent;
    EventType[GatewayEvent.ReconnectEvent] = ReconnectEvent;
    EventType[GatewayEvent.HelloEvent] = HelloEvent;
    EventType[GatewayEvent.HeartbeatAckEvent] = HeartbeatAckEvent;

    let identifier = payload.t ? `${payload.op}-${payload.t}` : `${payload.op}`;
    if (EventType[identifier] !== undefined) {
        return new EventType[identifier](payload.d, payload.s, payload.t);
    } else {
        return UNKNOWN_EVENT;
    }
}
