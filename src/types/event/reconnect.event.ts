import { GatewayEvent } from '../gateway/event';
import { Event } from './event';

export class ReconnectEvent extends Event<null> {
    code: number = Number.parseInt(GatewayEvent.ReconnectEvent);

    constructor() {
        super(null);
    }
}
