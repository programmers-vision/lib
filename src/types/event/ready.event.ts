import { ReadyData } from '../gateway/ready.data';
import { Event } from '@/types/event/event';

export class ReadyEvent extends Event<ReadyData> {
    constructor(payload: ReadyData, sequence: number, tag: string) {
        super(payload, sequence, tag);
    }
}
