import { GatewayPayload } from '@/types/gateway/payload';
import { parseEvent } from '@/client/event.mapper';

test('maps raw data to event', () => {
    const input: GatewayPayload[] = [
        {
            t: 'READY',
            s: 1,
            op: 0,
            d: {
                v: 9,
                user_settings: {},
                user: {
                    verified: true,
                    username: 'Vision Bot | Dev',
                    mfa_enabled: true,
                    id: '863539028829143060',
                    flags: 0,
                    email: null,
                    discriminator: '2871',
                    bot: true,
                    avatar: '3fa590d3afe5484d5be82b0325fa0a9c'
                },
                session_id: '1e14093fe8897989e01e2d387b2f8a05',
                relationships: [],
                private_channels: [],
                presences: [],
                guilds: [],
                guild_join_requests: [],
                geo_ordered_rtc_regions: [
                    'europe',
                    'russia',
                    'india',
                    'us-east',
                    'us-central'
                ],
                application: { id: '863539028829143060', flags: 262144 },
                _trace: [
                    '["gateway-prd-main-dbh6",{micros:175406,calls:["discord-sessions-blue-prd-2-157",{micros:171990,calls:["start_session",{micros:82826,calls:["api-prd-main-ghsm",{micros:76723,calls:["get_user",{micros:10323},"add_authorized_ip",{micros:7807},"get_guilds",{micros:20197},"coros_wait",{micros:0}]}]},"guilds_connect",{micros:1,calls:[]},"presence_connect",{micros:40976,calls:[]}]}]}]'
                ]
            }
        },
        {
            t: 'GUILD_MEMBER_UPDATE',
            s: 2,
            op: 0,
            d: {
                user: {
                    username: 'Vision Bot | Dev',
                    id: '863539028829143060',
                    discriminator: '2871',
                    bot: true,
                    avatar: '3fa590d3afe5484d5be82b0325fa0a9c'
                },
                roles: ['864943995871166475'],
                premium_since: null,
                pending: false,
                nick: null,
                mute: false,
                joined_at: '2021-07-14T18:58:38.528483+00:00',
                is_pending: false,
                hoisted_role: null,
                guild_id: '846016146843238410',
                deaf: false,
                avatar: null
            }
        },
        {
            t: 'MESSAGE_CREATE',
            s: 3,
            op: 0,
            d: {
                type: 7,
                tts: false,
                timestamp: '2021-07-14T18:58:38.567000+00:00',
                pinned: false,
                mentions: [],
                mention_roles: [],
                mention_everyone: false,
                member: {
                    roles: ['864943995871166475'],
                    premium_since: null,
                    pending: false,
                    nick: null,
                    mute: false,
                    joined_at: '2021-07-14T18:58:38.528483+00:00',
                    is_pending: false,
                    hoisted_role: null,
                    deaf: false,
                    avatar: null
                },
                id: '864943996160442378',
                flags: 0,
                embeds: [],
                edited_timestamp: null,
                content: '',
                components: [],
                channel_id: '846016146843238413',
                author: {
                    username: 'Vision Bot | Dev',
                    public_flags: 0,
                    id: '863539028829143060',
                    discriminator: '2871',
                    bot: true,
                    avatar: '3fa590d3afe5484d5be82b0325fa0a9c'
                },
                attachments: [],
                guild_id: '846016146843238410'
            }
        },
        {
            t: 'MESSAGE_UPDATE',
            s: 5,
            op: 0,
            d: {
                type: 0,
                tts: false,
                timestamp: '2021-07-14T18:58:52.491000+00:00',
                pinned: false,
                mentions: [],
                mention_roles: [],
                mention_everyone: false,
                member: {
                    roles: [],
                    mute: false,
                    joined_at: '2021-05-23T13:26:07.676000+00:00',
                    hoisted_role: null,
                    deaf: false
                },
                id: '864944054562717746',
                flags: 0,
                embeds: [],
                edited_timestamp: '2021-07-14T18:58:58.292063+00:00',
                content: 'Edit',
                components: [],
                channel_id: '846016146843238413',
                author: {
                    username: 'イオタ-天才',
                    public_flags: 64,
                    id: '756757056941326397',
                    discriminator: '3316',
                    avatar: 'a7341821894a7b208c0ec47ee51eacb8'
                },
                attachments: [],
                guild_id: '846016146843238410'
            }
        },
        {
            t: 'MESSAGE_DELETE',
            s: 6,
            op: 0,
            d: {
                id: '864944054562717746',
                channel_id: '846016146843238413',
                guild_id: '846016146843238410'
            }
        },
        { op: 1 },
        {
            op: 2,
            d: {
                token: '<token>',
                intents: 513,
                properties: {
                    $os: 'linux',
                    $browser: 'vision-lib',
                    $device: 'vision-lib'
                }
            }
        },
        { op: 10, d: { heartbeat_interval: 45000 } },
        { op: 11 }
    ];

    const processed = input.map(parseEvent).map((e) => e.getPOJO());

    expect(processed).toStrictEqual(input);
});
