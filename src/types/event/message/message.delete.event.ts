import { Event } from '@/types/event/event';
import { GatewayMessageReference } from '@/types/gateway/message-reference';

export class MessageDeleteEvent extends Event<GatewayMessageReference> {
    constructor(data: GatewayMessageReference, seq: number, evt: string) {
        super(data, seq, evt);
    }
}
