import { GatewayActivity } from './activity';

export interface GatewayPresence {
    since: number;
    activities: GatewayActivity[];
    status: string;
    afk: boolean;
}
