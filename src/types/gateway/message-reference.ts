/**
 * MessageReference. Noticed in both: Deletion and mentioning
 * other messages. The difference is that 'MESSAGE_DELETE' uses
 * `id` and 'MESSAGE_CREATE' (with a reference) uses `message_id`
 * leaving us with the choice to either create 2 interfaces or
 * 1 less strictly typed. For now I decided to go with latter, but
 * this is subject to change!
 */
export interface GatewayMessageReference {
    id?: string;
    message_id?: string;
    channel_id: string;
    guild_id: string;
}
