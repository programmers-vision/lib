export enum GatewayEvent {
    Unknown = '-1',
    ReadyEvent = '0-READY',
    MemberUpdateEvent = '0-GUILD_MEMBER_UPDATE',
    MessageCreateEvent = '0-MESSAGE_CREATE',
    MessageUpdateEvent = '0-MESSAGE_UPDATE',
    MessageDeleteEvent = '0-MESSAGE_DELETE',
    HeartbeatEvent = '1',
    IdentifyEvent = '2',
    ResumeEvent = '6',
    ReconnectEvent = '7',
    HelloEvent = '10',
    HeartbeatAckEvent = '11'
}
