export interface GatewayUser {
    verified: boolean;
    username: string;
    mfa_enabled: boolean;
    id: string;
    flags: number;
    public_flags?: number;
    email: string | null;
    discriminator: string;
    bot: boolean;
    avatar: string | null;
}
