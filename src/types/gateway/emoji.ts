export interface GatewayEmoji {
    name: string;
    id?: string;
    animated?: string;
}
