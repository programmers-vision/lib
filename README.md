# Vision TypeScript Library for Discord

The "Vision" TypeScript library for Discord is a modern and (soon to be)
up-to-date Discord API library for TypeScript. It will provide the most
important types, classes, and methods while remaining very lightweight yet
powerful.  
One of the biggest differences to the rather popular
[discord.js](https://discord.js.org/?source=post_page/#/) library is the
the capability of easily handling different types of events inside a single
handler, so you won't have to split your application logic by event types,
but are free to design it as you wish and handling the events you want inside
the event handler you code. Another major functional difference is that
discord.js trims the objects to their need before passing on the message data
into your application, meaning that some properties, such as the new v9
[message components](https://discord.com/developers/docs/interactions/message-components),
which, when using their library, won't be passed along with the original
message data. This behavior is avoided with this library since we merely
assign types or do minor modifications to the data structure instead of
completely transforming it.

## Feature Set
At this point the library provides means for a bot client to connect to the
Discord gateway and listen to events, while staying connected. Events are
then parsed into appropriate types. 
Currently supported are all "Pre-Ready" Events and:
- Ready Event
- Member Update Event
- Message Create Event
- Message Update Event
- Message Delete Event  

There are also out-of-the-box event handlers, which can subscribe to the
event broadcast in the client. Once coding an Event Handler, simply add
it to the listeners and it will fetch events from that point on.  
Given that the Bot only functions as long as there's a heartbeat going on 
between client and gateway, there is the default HeartbeatHandler, as well
as an IdentificationHandler, which is also compulsory as it's needed later
on in the Discord Client Identification process.  
You can attach your Event Handlers after those two in order to interact with
the Discord API. The functionality of those Handlers is as limited as the 
amount of typed events, which is why we will continously work on correctly
and fully typing the incoming data!

## Issues & Feature Requests

Since this library is mainly maintained to improve code quality and allow
the usage of newer components inside the "Vision Bot" feature set, at least
in the beginning, will be quite targetted towards that bot and likely won't
include every feature possible in Discord. Thus feel free to request features
or search for bugs and report them in the
[Issues Tab](https://gitlab.com/programmers-vision/lib/-/issues) of this
repository. As we will build the Vision Bot the feature set will naturally
grow as we will integrate more parts of the Discord API into the bot, so
you also can just stay patient and the feature you want to have will,
eventually, be implemented in this library.
