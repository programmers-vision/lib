import { GatewayEvent } from '../gateway/event';
import { Event } from './event';

export class HeartbeatEvent extends Event<number | null> {
    code: number = Number.parseInt(GatewayEvent.HeartbeatEvent);

    constructor(seq: number | null) {
        super(seq);
    }
}
