import { GatewayUser } from './user';

export interface ReadyData {
    v: number;
    user_settings: any;
    user: GatewayUser;
    session_id: string;
    relationships: any[];
    private_channels: any[];
    presences: any[];
    guilds: [];
    guild_join_requests: any[];
    geo_ordered_rtc_regions: string[];
    application: { id: string; flags: number };
}
