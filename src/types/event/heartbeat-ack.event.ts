import { GatewayEvent } from '../gateway/event';
import { Event } from './event';

export class HeartbeatAckEvent extends Event {
    code: number = Number.parseInt(GatewayEvent.HeartbeatAckEvent);
}
