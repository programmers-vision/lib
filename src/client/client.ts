import * as WebSocket from 'ws';

import { Event } from '@/types/event/event';
import { ConnectionOptions } from '@/types/gateway/connection.data';
import { GatewayPayload } from '@/types/gateway/payload';
import { parseEvent } from './event.mapper';
import { EventHandler } from './handlers/event.handler';
import { HeartbeatHandler } from './handlers/heartbeat.handler';
import { DEBUG, GATEWAY_URL } from '@/constant';
import { IdentificationHandler } from './handlers/identification.handler';
import { DebugHandler } from './handlers/debug.handler';

export class Client {
    ws?: WebSocket;
    token: string;
    connected = false;
    subscribers: EventHandler[] = [];

    constructor(token: string) {
        this.token = token;
        this.subscribe(new HeartbeatHandler(this));
        this.subscribe(new IdentificationHandler(this));
        if (DEBUG) this.subscribe(new DebugHandler(this));
    }

    /**
     * Establishes a Discord gateway connection using
     * provided options.
     * @param options gateway connection options
     */
    connect(options: ConnectionOptions) {
        const v = `v=${options.gatewayVersion}`;
        const enc = `encoding=${options.encoding}`;
        const comp = options.compress ? `&compress=${options.compress}` : '';
        this.ws = new WebSocket(`${GATEWAY_URL}?${v}&${enc}${comp}`);
        this.ws.on('message', (data) => {
            //console.log(`IN: ${data}`); //TODO: Remove | Debug Only
            this.pushEvent(JSON.parse(data.toString()));
        });
        this.connected = true;
    }

    /**
     * Sends a Payload to the Discord Gateway
     * @param payload
     */
    send(payload: GatewayPayload) {
        const data = JSON.stringify(payload);
        //console.log(`OUT: ${data}`);  //TODO: Remove | Debug Only
        if (this.connected) this.ws!.send(data);
    }

    /**
     * Adds Event Handlers to the Subscribers, which will
     * receive all events, as soon as events come in.
     * @param handlers event handlers
     */
    subscribe(...handlers: EventHandler[]) {
        this.subscribers.push(...handlers);
    }

    /**
     * Parses raw incoming gateway data into a
     * strictly typed event object and provides it
     * to all handlers.
     * @param payload incoming gateway data
     */
    async pushEvent(payload: GatewayPayload) {
        const event: Event<unknown> = parseEvent(payload);

        for (const subscriber of this.subscribers) {
            const consider = await subscriber.shouldProcess(event);
            if (consider) {
                const consumed = await subscriber.process(event);
                if (consumed) break;
            }
        }
    }
}
