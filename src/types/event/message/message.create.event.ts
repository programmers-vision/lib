import { Event } from '@/types/event/event';
import { GatewayMessage } from '@/types/gateway/message';

export class MessageCreateEvent extends Event<GatewayMessage> {
    constructor(data: GatewayMessage, seq: number, evt: string) {
        super(data, seq, evt);
    }
}
