export class ConnectionOptions {
    gatewayVersion = 9;
    encoding: 'json' | 'etf' = 'json';
    compress?: 'zlib-stream';
}
