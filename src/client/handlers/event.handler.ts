import { Event } from '@/types/event/event';
import { Client } from '@/client/client';

export interface EventHandler {
    client: Client;

    /**
     * Decides whether an Event should be handled,
     * by this Event Handler. When the Promise
     * resolves to `true`, EventHandler#process is
     * called, otherwise nothing happens.
     * @param event to be inspected
     * @returns whether event will be handled
     */
    shouldProcess(event: Event<unknown>): Promise<boolean>;

    /**
     * Processes the Event with the current Event
     * Handler.
     * @param event to be processed
     * @returns whether the event should be consumed
     */
    process(event: Event<unknown>): Promise<boolean>;
}
