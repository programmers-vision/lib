import { GatewayPayload } from '@/types/gateway/payload';
import { GatewayEvent } from '../gateway/event';

export class Event<T = undefined> {
    code: number = 0;
    data?: T;
    sequenceNumber?: number;
    eventName?: string;

    constructor(d?: T, s?: number, t?: string) {
        this.data = d;
        this.sequenceNumber = s;
        this.eventName = t;
    }

    getPOJO(): GatewayPayload {
        const pojo = { op: this.code };
        if (this.data !== undefined) Object.assign(pojo, { d: this.data });
        if (this.sequenceNumber !== undefined)
            Object.assign(pojo, { s: this.sequenceNumber });
        if (this.eventName) Object.assign(pojo, { t: this.eventName });
        return pojo;
    }
}

class UnknownEvent extends Event {
    code = Number.parseInt(GatewayEvent.Unknown);
    eventName = 'Unknown Event';

    send(ws?: WebSocket) {}
}

export const UNKNOWN_EVENT = new UnknownEvent();
