import { GatewayUser } from './user';

export interface GatewayMember {
    user: Partial<GatewayUser>;
    roles: string[];
    premium_since: any;
    pending: boolean;
    nick: string | null;
    mute: boolean;
    joined_at: Date;
    is_pending: boolean;
    hoisted_role: any;
    guild_id: string;
    deaf: boolean;
    avatar: any;
}
