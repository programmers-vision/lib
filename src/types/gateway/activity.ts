import { GatewayEmoji } from './emoji';

export enum ActivityType {
    GAME,
    STREAMING,
    LISTENING,
    WATCHING,
    CUSTOM,
    COMPETING
}

export enum ActivityFlags {
    INSTANCE,
    JOIN,
    SPECTATE,
    JOIN_REQUEST,
    SYNC,
    PLAY
}

export interface ActivityTimestamps {
    start?: number;
    end?: number;
}

export interface ActivityParty {
    id?: string;
    size?: number;
}

export interface ActivityAssets {
    large_image?: string;
    large_text?: string;
    small_image?: string;
    small_text?: string;
}

export interface ActivitySecrets {
    join?: string;
    spectate?: string;
    match?: string;
}

export interface ActivityButton {
    label: string;
    url: string;
}

export interface GatewayActivity {
    name: string;
    type: ActivityType;
    url?: string;
    created_at: number;
    timestamps?: ActivityTimestamps;
    application_id?: string;
    details?: string;
    state?: string;
    emoji?: GatewayEmoji;
    party?: ActivityParty;
    assets?: ActivityAssets;
    secrets: ActivitySecrets;
    instance?: boolean;
    flags?: number;
    buttons?: ActivityButton[];
}

export function computeFlags(...flags: ActivityFlags[]) {
    return flags.reduce((a, b) => (1 << a) + (1 << b));
}
