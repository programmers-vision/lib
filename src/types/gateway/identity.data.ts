import * as os from 'os';

import { GatewayPresence } from './presence';

export class IdentityOptions {
    token: string;
    properties: IdentityProperties = new IdentityProperties();
    compress = false;
    large_threshold = 50;
    shard?: number[];
    presence?: GatewayPresence;
    intents: number = 1 << 9;

    constructor(token: string) {
        this.token = token;
    }
}

class IdentityProperties {
    $os: string = os.platform();
    $browser = 'vision-lib';
    $device = 'vision-lib';
}
