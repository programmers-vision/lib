import { IdentityOptions } from '@/types/gateway/identity.data';
import { GatewayEvent } from '../gateway/event';
import { Event } from './event';

export class IdentifyEvent extends Event<IdentityOptions> {
    code: number = Number.parseInt(GatewayEvent.IdentifyEvent);

    constructor(data: IdentityOptions) {
        super(data);
    }
}
