import { Event } from '@/types/event/event';
import { GatewayMember } from '@/types/gateway/member';

export class MemberUpdateEvent extends Event<GatewayMember> {
    constructor(data: GatewayMember, seq: number, evt: string) {
        super(data, seq, evt);
    }
}
