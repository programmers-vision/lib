import { Event, UNKNOWN_EVENT } from '@/types/event/event';
import { Client } from '@/client/client';
import { EventHandler } from './event.handler';

export class DebugHandler implements EventHandler {
    client: Client;

    constructor(client: Client) {
        this.client = client;
    }

    async shouldProcess(): Promise<boolean> {
        return true;
    }

    async process(event: Event<unknown>): Promise<boolean> {
        if (event !== UNKNOWN_EVENT) console.log(JSON.stringify(event));
        else console.log(`Unknown Event unhandled!`);
        return false;
    }
}
