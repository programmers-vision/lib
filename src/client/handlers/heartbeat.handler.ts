import { Event } from '@/types/event/event';
import { HeartbeatEvent } from '@/types/event/heartbeat.event';
import { HelloEvent } from '@/types/event/hello.event';
import { Client } from '@/client/client';
import { EventHandler } from './event.handler';

export class HeartbeatHandler implements EventHandler {
    client: Client;
    interval?: number;
    thread?: NodeJS.Timer;
    seq: number | null = null;

    constructor(client: Client) {
        this.client = client;
    }

    async shouldProcess(event: Event<unknown>): Promise<boolean> {
        return true;
    }

    async process(event: Event<unknown>): Promise<boolean> {
        const response = new HeartbeatEvent(this.seq).getPOJO();

        if (event.sequenceNumber) this.seq = event.sequenceNumber;

        if (event instanceof HelloEvent) {
            this.interval = event.data!.heartbeat_interval;
            this.client.send(response);
            this.thread = setTimeout(() => {
                this.client.send(response);
            }, this.interval);
            return true;
        } else if (event instanceof HeartbeatEvent) {
            this.client.send(response);
            return true;
        }
        return false;
    }
}
