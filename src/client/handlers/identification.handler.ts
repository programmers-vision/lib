import { Event } from '@/types/event/event';
import { Client } from '@/client/client';
import { EventHandler } from './event.handler';
import { ReconnectEvent } from '@/types/event/reconnect.event';
import { ResumeEvent } from '@/types/event/resume.event';
import { HeartbeatAckEvent } from '@/types/event/heartbeat-ack.event';
import { IdentityOptions } from '@/types/gateway/identity.data';
import { IdentifyEvent } from '@/types/event/identify.event';
import { ReadyEvent } from '@/types/event/ready.event';

export class IdentificationHandler implements EventHandler {
    client: Client;
    identified = false;

    token: string;
    session_id?: string;
    seq: number | null = null;

    constructor(client: Client) {
        this.client = client;
        this.token = client.token;
    }

    async shouldProcess(event: Event<unknown>): Promise<boolean> {
        return true;
    }

    async process(event: Event<unknown>): Promise<boolean> {
        if (event.sequenceNumber) this.seq = event.sequenceNumber;

        if (event instanceof ReconnectEvent) {
            const resume = new ResumeEvent({
                token: this.token!,
                session_id: this.session_id!,
                seq: this.seq
            });
            this.client.send(resume.getPOJO());
            return true;
        } else if (event instanceof HeartbeatAckEvent) {
            const id = new IdentityOptions(this.token);
            const packet = new IdentifyEvent(id);
            this.client.send(packet.getPOJO());
            return true;
        } else if (event instanceof ReadyEvent) {
            this.session_id = event.data!.session_id;
        }
        return false;
    }
}
