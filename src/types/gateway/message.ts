import { GatewayMember } from './member';
import { GatewayMessageReference } from './message-reference';
import { GatewayUser } from './user';

/**
 * Just a little thought for this one:
 * Does this payload continue to grow when you
 * reference a message, which contains a message
 * reference?
 */
export interface GatewayMessage {
    type: number; //TODO: Enum
    tts: boolean;
    timestamp: Date;
    referenced_message?: GatewayMessage;
    message_reference: GatewayMessageReference;
    nonce?: string;
    pinned: boolean;
    mentiones: any[];
    mention_roles: any[];
    mention_everyone: boolean;
    member: Partial<GatewayMember>;
    id: string;
    flags: number;
    embeds: any[];
    edited_timestamp: Date | null;
    content: string;
    components: any[];
    channel_id: string;
    author: Partial<GatewayUser>;
    attachments: any[];
    guild_id: string;
}
