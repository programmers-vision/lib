import { GatewayEvent } from '../gateway/event';
import { Event } from './event';

export class ResumeEvent extends Event<ResumePayload> {
    code: number = Number.parseInt(GatewayEvent.ResumeEvent);

    constructor(data: ResumePayload) {
        super(data);
    }
}

interface ResumePayload {
    token: string;
    session_id: string;
    seq: number | null;
}
